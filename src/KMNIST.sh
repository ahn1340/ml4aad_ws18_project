#!/usr/bin/env bash
#!/bin/bash
#SBATCH -p mlstudents_cpu-ivy
#SBATCH --mem 16000
#SBATCH -t 2-00:00 # time (D-HH:MM)
#SBATCH -c 1 # number of cores
#SBATCH -a 1-2 # array size
#SBATCH --gres=gpu:0  # no gpu
#SBATCH -o log_KMNIST/%x.%N.%A.%a.out # STDOUT  (the folder log has to exist) %A will be replaced by the SLURM_ARRAY_JOB_ID value, whilst %a will be replaced by the SLURM_ARRAY_TASK_ID
#SBATCH -e log_KMNIST/%x.%N.%A.%a.err # STDERR  (the folder log has to exist) %A will be replaced by the SLURM_ARRAY_JOB_ID value, whilst %a will be replaced by the SLURM_ARRAY_TASK_ID

source activate ml4aad

if [ $SLURM_ARRAY_TASK_ID -eq 1 ]; then
    python run_BOHB_cluster.py --run_id "$SLURM_ARRAY_JOB_ID" --nic_name eth0 --shared_directory runs_KMNIST --min_budget 2 --max_budget 20 --n_iterations 3 --n_workers 2 --dataset KMNIST --output_file "results_KMNIST_2_20_2_worker$SLURM_ARRAY_TASK_ID.pkl" --data_subset True
else
    python run_BOHB_cluster.py --run_id "$SLURM_ARRAY_JOB_ID" --nic_name eth0 --shared_directory runs_KMNIST --worker --min_budget 2 --max_budget 20 --n_iterations 3 --n_workers 2 --dataset KMNIST --output_file "results_KMNIST_2_20_2_worker$SLURM_ARRAY_TASK_ID.pkl" --data_subset True
fi
