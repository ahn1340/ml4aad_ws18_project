#!/usr/bin/env bash

run_ids="1 2 3 4"
epochs="1 2 3 4 5 6 7 8"
snapshots="1 2 3 "

for run_id in $run_ids; do
    for epoch in $epochs; do
        for s in $snapshots; do
            echo "$run_id $epoch $i "
            python visualize.py --gene "run${run_id}_epoch${epoch}_${s}" --save "gifs/run${run_id}/epoch${epoch}_${s}" --epoch $epoch
        done
    done
done
