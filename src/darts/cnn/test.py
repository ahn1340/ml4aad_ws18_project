import os
import sys
import glob
import numpy as np
import torch
import utils
import logging
import argparse
import torch.nn as nn
import genotypes
import torch.utils
import torchvision.datasets as dset
import torch.backends.cudnn as cudnn

from torch.autograd import Variable
from model import Network_KMNIST as Network
from datasets import KMNIST, K49
from torchvision import transforms


parser = argparse.ArgumentParser("KMNIST")
parser.add_argument('--data', type=str, default='../data', help='location of the data corpus')
parser.add_argument('--batch_size', type=int, default=96, help='batch size')
parser.add_argument('--report_freq', type=float, default=50, help='report frequency')
parser.add_argument('--gpu', type=int, default=0, help='gpu device id')
parser.add_argument('--init_channels', type=int, default=36, help='num of init channels')
parser.add_argument('--layers', type=int, default=20, help='total number of layers')
parser.add_argument('--model_path', type=str, default='EXP/model.pt', help='path of pretrained model')
parser.add_argument('--auxiliary', action='store_true', default=False, help='use auxiliary tower')
parser.add_argument('--cutout', action='store_true', default=False, help='use cutout')
parser.add_argument('--cutout_length', type=int, default=16, help='cutout length')
parser.add_argument('--drop_path_prob', type=float, default=0.2, help='drop path probability')
parser.add_argument('--seed', type=int, default=0, help='random seed')
parser.add_argument('--arch', type=str, default='DARTS', help='which architecture to use')
parser.add_argument('--run_id', type=int, default=0, help='random seed')
parser.add_argument('--save_dir', type=str, default='test', help='file in which loss and accuracies are stored')
parser.add_argument('--dataset', type=str, default='KMNIST', help='dataset name')


args = parser.parse_args()

# Create experiment dir.
utils.create_exp_dir(args.save_dir)

log_format = '%(asctime)s %(message)s'
logging.basicConfig(stream=sys.stdout, level=logging.INFO,
                    format=log_format, datefmt='%m/%d %I:%M:%S %p')
fh = logging.FileHandler(os.path.join(args.save_dir, 'log.txt'))
fh.setFormatter(logging.Formatter(log_format))
logging.getLogger().addHandler(fh)



CIFAR_CLASSES = 10


def main():
  #if not torch.cuda.is_available():
    #logging.info('no gpu device available')
    #sys.exit(1)

  np.random.seed(args.seed)
  #torch.cuda.set_device(args.gpu)
  #cudnn.benchmark = True
  torch.manual_seed(args.seed)
  #cudnn.enabled=True
  #torch.cuda.manual_seed(args.seed)
  #logging.info('gpu device = %d' % args.gpu)
  logging.info("args = %s", args)

  # No data augmentation for this experiment
  data_augmentations = None

  if data_augmentations is None:
    # We only use ToTensor here as that is al that is needed to make it work
    data_augmentations = transforms.ToTensor()
  elif isinstance(type(data_augmentations), list):
    data_augmentations = transforms.Compose(data_augmentations)
  elif not isinstance(data_augmentations, transforms.Compose):
    raise NotImplementedError

    # Make data batch iterable
  if args.dataset == 'KMNIST':
    train_dataset = KMNIST(args.data, 'train', data_augmentations)
    validation_dataset = KMNIST(args.data, 'val', data_augmentations)
    test_dataset = KMNIST(args.data, 'test', data_augmentations)
  elif args.dataset == 'K49':
    train_dataset = K49(args.data, 'train', data_augmentations)
    validation_dataset = K49(args.data, 'val', data_augmentations)
    test_dataset = K49(args.data, 'test', data_augmentations)
  else:
    raise NotImplementedError

  train_loader = torch.utils.data.DataLoader(dataset=train_dataset,
                                             batch_size=args.batch_size,
                                             shuffle=True)
  validation_loader = torch.utils.data.DataLoader(dataset=validation_dataset,
                                                  batch_size=args.batch_size,
                                                  shuffle=False)
  test_loader = torch.utils.data.DataLoader(dataset=test_dataset,
                                             batch_size=args.batch_size,
                                             shuffle=True)

  genotype = eval("genotypes.%s" % args.arch)
  model = Network(args.init_channels, test_dataset.n_classes, genotype)
  #model = model.cuda()
  utils.load(model, args.model_path)

  logging.info("param size = %fMB", utils.count_parameters_in_MB(model))

  criterion = nn.CrossEntropyLoss()
  #criterion = criterion.cuda()

  model.drop_path_prob = args.drop_path_prob

  train_acc, train_obj = infer(train_loader, model, criterion)
  logging.info('train_acc %f', train_acc)

  val_acc, val_obj = infer(validation_loader, model, criterion)
  logging.info('val_acc %f', val_acc)

  test_acc, test_obj = infer(test_loader, model, criterion)
  logging.info('test_acc %f', test_acc)

  test_file = 'test_result.txt'
  train_file = 'train_result.txt'
  val_file = 'val_result.txt'
  with open(os.path.join(args.save_dir, train_file), "a+") as f:
    f.write("{} {}\n".format(str(train_acc), str(train_obj)))
  with open(os.path.join(args.save_dir, val_file), "a+") as f:
    f.write("{} {}\n".format(str(val_acc), str(val_obj)))
  with open(os.path.join(args.save_dir, test_file), "a+") as f:
    f.write("{} {}\n".format(str(test_acc), str(test_obj)))



def infer(test_queue, model, criterion):
  objs = utils.AvgrageMeter()
  top1 = utils.AvgrageMeter()
  model.eval()

  for step, (input, target) in enumerate(test_queue):
    #input = Variable(input, volatile=True).cuda()
    #target = Variable(target, volatile=True).cuda(async=True)
    input = Variable(input, volatile=True)
    target = Variable(target, volatile=True)

    logits, _ = model(input)
    loss = criterion(logits, target)

    prec1 = utils.accuracy(logits, target, topk=(1, ))[0]
    n = input.size(0)
    objs.update(loss.data[0], n)
    top1.update(prec1.data[0], n)

    if step % args.report_freq == 0:
      logging.info('evaluation [%03d|%03d] loss: %e acc_top1: %f', step, len(test_queue), objs.avg, top1.avg)

  return top1.avg, objs.avg


if __name__ == '__main__':
  main() 

