#!/usr/bin/env bash
#!/bin/bash
#SBATCH -p mlstudents_cpu-ivy
#SBATCH --mem 16000
#SBATCH -t 2-00:00 # time (D-HH:MM)
#SBATCH -c 1 # number of cores
##SBATCH --gres=gpu:0  # no gpu
#SBATCH -o log_K49/%x.%N.%A.out # STDOUT  (the folder log has to exist) %A will be replaced by the SLURM_ARRAY_JOB_ID value, whilst %a will be replaced by the SLURM_ARRAY_TASK_ID
#SBATCH -e log_K49/%x.%N.%A.err # STDERR  (the folder log has to exist) %A will be replaced by the SLURM_ARRAY_JOB_ID value, whilst %a will be replaced by the SLURM_ARRAY_TASK_ID

source activate darts

#rm -r search-EXP*

python train_search.py --epochs 1 --init_channels 16 --dataset K49 --batch_size 64 --report_freq 10
