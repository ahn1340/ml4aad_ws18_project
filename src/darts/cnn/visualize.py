import os
import sys
import argparse
import genotypes
from graphviz import Digraph


def plot(genotype, filename, epoch):
  g = Digraph(
      format='png',
      edge_attr=dict(fontsize='20', fontname="times"),
      node_attr=dict(style='filled', shape='rect', align='center', fontsize='20', height='0.5', width='0.5', penwidth='2', fontname="times"),
      engine='dot')
  g.body.extend(['rankdir=LR'])

  g.node("input", fillcolor='darkseagreen2')
  #steps = len(genotype) // 2
  steps = 4

  for i in range(steps):
    g.node(str(i), fillcolor='lightblue')

  # First node with one input
  op, j = genotype[0]
  if op != 'none':
    g.edge("input", str(0), label=op, fillcolor="gray")
  for i in range(1, steps):
    for k in [2*i - 1, 2*i]:
      op, j = genotype[k]
      if j == 0:
        u = "input"
      else:
        u = str(j-1)
      v = str(i)
      if op != 'none':
        g.edge(u, v, label=op, fillcolor="gray")

  g.node("concat", fillcolor='palegoldenrod')
  for i in range(steps):
    g.edge(str(i), "concat", fillcolor="gray")
  g.node("output", fillcolor="darkseagreen2")
  g.edge("concat", "output", label="conv1x1")

  #add image caption
  g.attr(label="Epoch {}".format(epoch), overlap='false', fontsize='20')

  g.render(filename, view=False)
  os.remove(filename)


if __name__ == '__main__':
  parser = argparse.ArgumentParser("KMNIST")
  parser.add_argument('--gene', type=str, default='KMNIST', help='genotype')
  parser.add_argument('--save', type=str, default='', help='nothinrg')
  parser.add_argument('--epoch', type=str, default='', help='nothinrg')

  args = parser.parse_args()

  genotype_name = args.gene
  try:
    genotype = eval('genotypes.{}'.format(genotype_name))
  except AttributeError:
    print("{} is not specified in genotypes.py".format(genotype_name)) 
    sys.exit(1)

  plot(genotype.normal, args.save, args.epoch)


