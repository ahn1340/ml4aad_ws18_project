import torch
import torch.nn as nn
import torch.nn.functional as F
from operations import *
from torch.autograd import Variable
from genotypes import PRIMITIVES
from genotypes import Genotype


class MixedOp(nn.Module):

  def __init__(self, C, stride):
    super(MixedOp, self).__init__()
    self._ops = nn.ModuleList()
    for primitive in PRIMITIVES:
      op = OPS[primitive](C, stride, False)
      if 'pool' in primitive:
        op = nn.Sequential(op, nn.BatchNorm2d(C, affine=False))
      self._ops.append(op)

  def forward(self, x, weights):
    return sum(w * op(x) for w, op in zip(weights, self._ops))


class Cell(nn.Module):

  def __init__(self, steps, C):
    super(Cell, self).__init__()

    # Define stride here for now.
    stride = 1

    self._steps = steps

    self._ops = nn.ModuleList()
    self._bns = nn.ModuleList()

    # create dag
    for i in range(self._steps):
      for j in range(1+i):
        op = MixedOp(C, stride)
        self._ops.append(op)

    # 1D convolution for taking the weighted sum of all hidden representations
    # No nonlinearity is applied because this operation is only for concatenating
    # the final output to have matching channel dimension.
    self.out = nn.Conv2d(in_channels=C*steps, out_channels=C, kernel_size=1, stride=1, padding=0)

  def forward(self, s0, weights):
    states = [s0]
    offset = 0
    for i in range(self._steps):
      s = sum(self._ops[offset + j](h, weights[offset + j]) for j, h in enumerate(states))
      offset += len(states)
      states.append(s)

    # Concatenate all hidden representations and take weighted sum (across pixels)
    out = self.out(torch.cat(states[1:], dim=1))
    return out


class Network(nn.Module):

  def __init__(self, C, num_classes, layers, criterion, steps=4, multiplier=4, stem_multiplier=3):
    super(Network, self).__init__()
    self._C = C
    self._num_classes = num_classes
    self._layers = layers
    self._criterion = criterion
    self._steps = steps
    self._multiplier = multiplier

    # Apply conv - ReLU - BN to the input to change its shape from
    # (N, 1, 28, 28) to (N, C, 28, 28).
    self.preprocess = nn.Sequential(
      nn.Conv2d(1, C, kernel_size=3, stride=1, padding=1),
      nn.Tanh(),
      #nn.BatchNorm2d(C, affine=False),
    )

    self.cells = nn.ModuleList()
    for i in range(layers):
      cell = Cell(steps, C)
      self.cells += [cell]

    pool_size = 4  # output height and width after global pooling
    self.global_pooling = nn.AdaptiveMaxPool2d(pool_size)
    self.classifier = nn.Linear(C * pool_size**2, num_classes)

    # Create architecture weights.
    self._initialize_alphas()

  def new(self):
    #model_new = Network(self._C, self._num_classes, self._layers, self._criterion).cuda()
    model_new = Network(self._C, self._num_classes, self._layers, self._criterion)
    for x, y in zip(model_new.arch_parameters(), self.arch_parameters()):
        x.data.copy_(y.data)
    return model_new

  def forward(self, input):
    # Preprocess data to have C channels.
    input = self.preprocess(input)

    # For my experiment there is just one cell.
    assert len(self.cells) == 1
    weights = F.softmax(self.alphas_normal, dim=-1)
    output = self.cells[0](input, weights)
    output = self.global_pooling(output)
    logits = self.classifier(output.view(output.size(0),-1))

    return logits

  def _loss(self, input, target):
    logits = self(input)
    return self._criterion(logits, target) 

  def _initialize_alphas(self):
    k = sum(1 for i in range(self._steps) for n in range(1+i))
    num_ops = len(PRIMITIVES)

    self.alphas_normal = Variable(1e-3*torch.randn(k, num_ops), requires_grad=True)
    #self.alphas_reduce = Variable(1e-3*torch.randn(k, num_ops), requires_grad=True)
    self._arch_parameters = [self.alphas_normal]

  def arch_parameters(self):
    return self._arch_parameters

  def genotype(self):

    def _parse(weights):
      gene = []
      n = 1
      start = 0
      for i in range(self._steps):
        end = start + n
        W = weights[start:end].copy()
        #edges = sorted(range(i + 1), key=lambda x: -max(W[x][k] for k in range(len(W[x])) if k != PRIMITIVES.index('none')))[:2]
        #for j in edges:
        #  k_best = None
        #  for k in range(len(W[j])):
        #    if k != PRIMITIVES.index('none'):
        #      if k_best is None or W[j][k] > W[j][k_best]:
        #        k_best = k
        #  gene.append((PRIMITIVES[k_best], j))
        edges = sorted(range(i + 1), key=lambda x: -max(W[x][k] for k in range(len(W[x]))))[:2]
        for j in edges:
          k_best = None
          for k in range(len(W[j])):
            if k_best is None or W[j][k] > W[j][k_best]:
              k_best = k
          gene.append((PRIMITIVES[k_best], j))
        start = end
        n += 1
      return gene

    gene_normal = _parse(F.softmax(self.alphas_normal, dim=-1).data.cpu().numpy())
    #gene_reduce = _parse(F.softmax(self.alphas_reduce, dim=-1).data.cpu().numpy())

    #concat = range(2+self._steps-self._multiplier, self._steps+2)
    concat = range(1, self._steps+1)  # last node
    genotype = Genotype(
      normal=gene_normal, normal_concat=concat,
      #reduce=gene_reduce, reduce_concat=concat
    )
    return genotype

