#!/usr/bin/env bash
#SBATCH -p mlstudents_cpu-ivy
#SBATCH --mem 16000
#SBATCH -t 2-00:00 # time (D-HH:MM)
#SBATCH -c 1 # number of cores
#SBATCH --gres=gpu:0  # no gpu
#SBATCH -o test_KMNIST1/log.out # STDOUT
#SBATCH -e test_KMNIST1/log.err # STDERR



#run_ids="1 2 3 4 "
epochs=19
run_ids="0 "

for run_id in $run_ids; do
    for i in $(seq 0 $epochs); do
	    python test.py --batch_size 64 --report_freq 100 --init_channels 3 --arch default --model_path "run${run_id}/epoch${i}_weights.pt" --save_dir "test${run_id}" \
	    	--dataset KMNIST
    done
done

	    #python test.py --batch_size 64 --report_freq 100 --init_channels 16 --arch run${run_id}_epoch8_3 --model_path "run${run_id}/epoch${i}_weights.pt" --save_dir "test${run_id}" \
