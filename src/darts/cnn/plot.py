import matplotlib.pyplot as plt
import os

def plot(files, split, names=None):
    """
    files store [acc, loss] in each row
    :param files:
    :return:
    """

    for run_id, file in enumerate(files):
        epochs = []
        accs = []
        losses = []
        with open(file, "r") as f:
            for idx, line in enumerate(f):
                epochs.append(idx+1)  # start from 1 epoch
                line = line.split(" ")
                accs.append(float(line[0]))
                losses.append(float(line[1]))

            plt.plot(epochs, accs, label=name[run_id])

    # For KMNIST
    #plt.title("KMNIST {} accuracy".format(split))
    #plt.xticks(range(0, 21, 2))
    #plt.xlim(left=-0.5, right=20.5)
    #plt.ylim(top=100, bottom=82)
    #plt.ylabel("accuracy (%)")
    #plt.xlabel("epoch")
    #plt.grid()
    #plt.legend(loc="lower right")
    #plt.savefig("{} accuracy_KMNIST.png".format(split))

    # For K49
    plt.title("K49 {} accuracy".format(split))
    plt.xticks(range(0, 11, 1))
    plt.xlim(left=-0.5, right=10.5)
    plt.ylim(top=100, bottom=78)
    plt.ylabel("accuracy (%)")
    plt.xlabel("epoch")
    plt.grid()
    plt.legend(loc="best")
    plt.savefig("{} accuracy_K49.png".format(split))

    plt.show()


dataset = "K49"

if dataset == "KMNIST":
    for split in ['train', 'test']:
        files = []
        #files.append("K49_test3/{}_result.txt".format(split))
        #for run_id in range(3, 5):  # 0 is default config
        #    files.append(os.path.join('K49_test{}_KMNIST/{}_result.txt'.format(run_id, split)))
        for run_id in range(0, 9):  # 0 is default config
            files.append('KMNIST_test{}/{}_result.txt'.format(run_id, split))
        print(files)
        plot(files, split)
elif dataset == "K49":

    for split in ['train', 'test']:
        files = []
        name = []
        for run_id in [1, 3, 5]:
            files.append("K49_test{}/{}_result.txt".format(run_id, split))
            name.append("run{}".format(run_id))
        # append tests with KMNIST architectures
        files.append("K49_test3_KMNIST/{}_result.txt".format(split))
        files.append("K49_test4_KMNIST/{}_result.txt".format(split))
        files.append("K49_test5_KMNIST/{}_result.txt".format(split))
        name.append("run3_KMNIST")
        name.append("run4_KMNIST")
        name.append("run5_KMNIST")

        plot(files, split, name)


#files = ["eval_default_conv1/test_result.txt", 'eval_best_3/test_result.txt', 'eval_default/test_result.txt']
#plot(files)
#plot("eval_best_3/test_result.txt")