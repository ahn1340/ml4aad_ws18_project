#!/usr/bin/env bash

python train.py --batch_size 64 --init_channels 3 --report_freq 50 --epochs 10 --arch default --dataset K49 --save_dir run0_K49
