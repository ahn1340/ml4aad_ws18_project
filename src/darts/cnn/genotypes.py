from collections import namedtuple

Genotype = namedtuple('Genotype', 'normal normal_concat')

PRIMITIVES = [
    'none',
    'max_pool_3x3',
    'avg_pool_3x3',
    'skip_connect',
    'sep_conv_3x3',
    'sep_conv_5x5',
    'dil_conv_3x3',
    'dil_conv_5x5'
]

default = Genotype(normal=[('none', 0), ('none', 1), ('none', 0), ('none', 2), ('none', 1), ('skip_connect', 0), ('none', 1)], normal_concat=range(1, 5))

run1_epoch8_3 = Genotype(normal=[('dil_conv_3x3', 0), ('avg_pool_3x3', 1), ('avg_pool_3x3', 0), ('max_pool_3x3', 2), ('none', 0), ('avg_pool_3x3', 2), ('avg_pool_3x3', 1)], normal_concat=range(1, 5))
run2_epoch8_3 = Genotype(normal=[('skip_connect', 0), ('dil_conv_3x3', 0), ('sep_conv_5x5', 1), ('dil_conv_3x3', 0), ('dil_conv_5x5', 2), ('max_pool_3x3', 2), ('avg_pool_3x3', 3)], normal_concat=range(1, 5))
run3_epoch8_3 = Genotype(normal=[('sep_conv_5x5', 0), ('dil_conv_3x3', 0), ('dil_conv_5x5', 1), ('sep_conv_5x5', 0), ('sep_conv_5x5', 2), ('sep_conv_3x3', 1), ('avg_pool_3x3', 2)], normal_concat=range(1, 5))
run4_epoch8_3 = Genotype(normal=[('dil_conv_5x5', 0), ('dil_conv_5x5', 0), ('avg_pool_3x3', 1), ('sep_conv_5x5', 2), ('none', 1), ('avg_pool_3x3', 0), ('dil_conv_3x3', 1)], normal_concat=range(1, 5))
run5_epoch8_3 = Genotype(normal=[('skip_connect', 0), ('sep_conv_5x5', 1), ('avg_pool_3x3', 0), ('avg_pool_3x3', 0), ('dil_conv_3x3', 1), ('avg_pool_3x3', 2), ('sep_conv_5x5', 3)], normal_concat=range(1, 5))
run6_epoch8_3 = Genotype(normal=[('skip_connect', 0), ('sep_conv_5x5', 1), ('dil_conv_5x5', 0), ('dil_conv_5x5', 2), ('max_pool_3x3', 0), ('dil_conv_3x3', 3), ('dil_conv_3x3', 2)], normal_concat=range(1, 5))
run7_epoch8_3 = Genotype(normal=[('none', 0), ('sep_conv_5x5', 1), ('sep_conv_5x5', 0), ('dil_conv_3x3', 2), ('dil_conv_3x3', 0), ('dil_conv_3x3', 2), ('skip_connect', 3)], normal_concat=range(1, 5))
run8_epoch8_3 = Genotype(normal=[('sep_conv_5x5', 0), ('avg_pool_3x3', 0), ('max_pool_3x3', 1), ('sep_conv_5x5', 2), ('sep_conv_5x5', 1), ('skip_connect', 0), ('avg_pool_3x3', 1)], normal_concat=range(1, 5))


# K49
K49_run1_epoch2_12 = Genotype(normal=[('skip_connect', 0), ('dil_conv_3x3', 0), ('dil_conv_3x3', 1), ('skip_connect', 0), ('dil_conv_3x3', 2), ('dil_conv_3x3', 3), ('max_pool_3x3', 1)], normal_concat=range(1, 5))
K49_run2_epoch2_12 = Genotype(normal=[('none', 0), ('none', 0), ('sep_conv_3x3', 1), ('none', 0), ('sep_conv_3x3', 1), ('sep_conv_3x3', 2), ('max_pool_3x3', 3)], normal_concat=range(1, 5))
K49_run3_epoch2_12 = Genotype(normal=[('none', 0), ('skip_connect', 0), ('none', 1), ('none', 1), ('dil_conv_3x3', 0), ('sep_conv_3x3', 2), ('skip_connect', 0)], normal_concat=range(1, 5))
K49_run4_epoch2_12 = Genotype(normal=[('none', 0), ('none', 0), ('none', 1), ('skip_connect', 0), ('sep_conv_3x3', 1), ('dil_conv_3x3', 2), ('none', 1)], normal_concat=range(1, 5))
K49_run5_epoch2_12 = Genotype(normal=[('max_pool_3x3', 0), ('skip_connect', 1), ('sep_conv_5x5', 0), ('sep_conv_3x3', 2), ('skip_connect', 0), ('sep_conv_3x3', 1), ('max_pool_3x3', 2)], normal_concat=range(1, 5))

