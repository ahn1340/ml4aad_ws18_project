import logging
logging.basicConfig(level=logging.INFO)

from BOHB_Worker import BOHB_Worker as BOHB_Worker
import numpy as np
import hpbandster.core.nameserver as hpns
import hpbandster.core.result as hpres
from hpbandster.optimizers import BOHB as BOHB


min_budget = 1
max_budget = 9
n_iterations = 1

# Step 1. Start nameserver
address = '127.0.0.1'
run_id = 'test1'
NS = hpns.NameServer(run_id=run_id, host=address, port=None)
NS.start()

# Step 2 start worker
w = BOHB_Worker(sleep_interval=0, dataset="KMNIST", min_max_budget=(min_budget, max_budget), nameserver=address, run_id=run_id)
w.run(background=True)

# Step 3 run bohb
bohb = BOHB(configspace=w.get_configspace(),
            run_id=run_id,
            nameserver=address,
            min_budget=min_budget,
            max_budget=max_budget,
            )  #TODO: argumentize these budgets

res = bohb.run(n_iterations=n_iterations)  #TODO: argumentize

# step 4 shutdown after bohb finished
bohb.shutdown(shutdown_workers=True)
NS.shutdown()

# step 5 Analysis
id2config = res.get_id2config_mapping()
incumbent = res.get_incumbent_id()

print('Best found configuration:', id2config[incumbent]['config'])
print('A total of %i unique configurations where sampled.' % len(id2config.keys()))
print('A total of %i runs where executed.' % len(res.get_all_runs()))
print('Total budget corresponds to %.1f full function evaluations.'%(sum([r.budget for r in res.get_all_runs()])/max_budget))



