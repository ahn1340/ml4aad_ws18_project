import logging
logging.basicConfig(level=logging.INFO)

import argparse
import pickle
import time
import os

from BOHB_Worker import BOHB_Worker as BOHB_Worker
import numpy as np
import hpbandster.core.nameserver as hpns
import hpbandster.core.result as hpres
from hpbandster.optimizers import BOHB as BOHB
import numpy as np


parser = argparse.ArgumentParser(description='Running BOHB on MNIST and K49')
parser.add_argument('--dataset',   type=str, help='Minimum budget used during the optimization.',    default="KMNIST")
parser.add_argument('--min_budget',   type=float, help='Minimum budget used during the optimization.',    default=9)
parser.add_argument('--max_budget',   type=float, help='Maximum budget used during the optimization.',    default=243)
parser.add_argument('--n_iterations', type=int,   help='Number of iterations performed by the optimizer', default=4)
parser.add_argument('--n_workers', type=int,   help='Number of workers to run in parallel.', default=2)
parser.add_argument('--worker', help='Flag to turn this into a worker process', action='store_true')
parser.add_argument('--run_id', type=str, help='A unique run id for this optimization run. An easy option is to use the job id of the clusters scheduler.')
parser.add_argument('--nic_name',type=str, help='Which network interface to use for communication.')
parser.add_argument('--shared_directory',type=str, help='A directory that is accessible for all processes, e.g. a NFS share.')
parser.add_argument('--data_subset',type=str, help='Whether to use data subset for smaller budgets or not')
parser.add_argument('--output_file',type=str, help='Name of the output pickle file', default="results.pkl")


args = parser.parse_args()

# Host name needed for every process
host = hpns.nic_name_to_host(args.nic_name)

if args.data_subset == "True":
    min_max_budget = (args.min_budget, args.max_budget)
else:
    min_max_budget = None

if args.worker:
    time.sleep(5)
    w = BOHB_Worker(run_id=args.run_id,
                    sleep_interval=0.5,
                    dataset=args.dataset,
                    min_max_budget=min_max_budget,
                    host=host,
                    id=np.random.randint(0, 100))  # TODO: id can be arguments in sh script.
    w.load_nameserver_credentials(working_directory=args.shared_directory)
    w.run(background=False)
    exit(0)

# Step 1. Start nameserver
NS = hpns.NameServer(run_id=args.run_id, host=host, port=0, working_directory=args.shared_directory)
ns_host, ns_port = NS.start()

# Step 2 Optimizers? TODO: What is exactly different between this and part in args.worker?
w = BOHB_Worker(sleep_interval=0.5,
                dataset=args.dataset,
                min_max_budget=min_max_budget,
                run_id=args.run_id,
                nameserver=ns_host,
                nameserver_port=ns_port,
                )
w.run(background=True)

# Step 3 run optimizer (I will use bohb)
bohb = BOHB(configspace=BOHB_Worker.get_configspace(),
            run_id=args.run_id,
            host=host,
            nameserver=ns_host,
            nameserver_port=ns_port,
            min_budget=args.min_budget,
            max_budget=args.max_budget,
            )

res = bohb.run(n_iterations=args.n_iterations, min_n_workers=args.n_workers)

# step 4 pickle dump all results
with open(os.path.join(args.shared_directory, args.output_file), 'wb') as fh:
    pickle.dump(res, fh)

# step 5 shutdown after bohb finished
bohb.shutdown(shutdown_workers=True)
NS.shutdown()

## step 6 Analysis
#id2config = res.get_id2config_mapping()
#incumbent = res.get_incumbent_id()
#
#print('Best found configuration:', id2config[incumbent]['config'])
#print('A total of %i unique configurations where sampled.' % len(id2config.keys()))
#print('A total of %i runs where executed.' % len(res.get_all_runs()))
#print('Total budget corresponds to %.1f full function evaluations.'%(sum([r.budget for r in res.get_all_runs()])/max_budget))



