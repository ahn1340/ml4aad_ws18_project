python main.py \
	--dataset=KMNIST \
	--epoch=20 \
	--batch_size=256 \
	--data_dir=../data \
	--learning_rate=0.001 \
	--training_loss=cross_entropy \
	--optimizer=adam \
	--model_path=../model \
	--verbose=INFO

