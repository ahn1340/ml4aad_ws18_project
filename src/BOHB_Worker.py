import ConfigSpace as CS
import ConfigSpace.hyperparameters as CSH
from hpbandster.core.worker import Worker
import torch
from main import train


class BOHB_Worker(Worker):
    def __init__(self, *args, sleep_interval=0, dataset='KMNIST', min_max_budget=None, **kwargs):
        super().__init__(*args, **kwargs)
        self.sleep_interval = sleep_interval
        self.dataset = dataset
        self.min_max_budget = min_max_budget
        if self.min_max_budget is not None:
            self.min_budget = min_max_budget[0]
            self.max_budget = min_max_budget[1]

        # for testing
        #self.dataset = "K49"

    def compute(self, config, budget, **kwargs):
        """
        Evaluate given config and return loss
        :param config: config dict
        :param budget: number of epochs
        :param min_budget: for computing the data subset percentage
        :param max_budget: for computing the data subset percentage
        :param kwargs:
        :return:
        """
        # helper dicts
        loss_dict = {'cross_entropy': torch.nn.CrossEntropyLoss,
                     'mse': torch.nn.MSELoss}
        opti_dict = {'adam': torch.optim.Adam,
                     'adad': torch.optim.Adadelta,
                     'sgd': torch.optim.SGD}

        # Let our budget be the number of epochs plus the datasubset percentage
        # calculated by normalizing the min-max budget to a percentage in[0.2, 1],
        # and use only that percentage of random data subset for training.
        if self.min_max_budget is not None:
            p_subset = 0.8 * (budget - self.min_budget) / (self.max_budget - self.min_budget) + 0.2
            #print(self.min_budget)
            #print(self.max_budget)
        else:
            p_subset = 1

        # compute the loss given budget
        train_score, val_score, test_score, _, _, _ = \
            train(self.dataset,
                  config,
                  data_dir="../data",
                  batch_size=config['batch_size'],
                  num_epochs=int(budget),
                  p_subset=p_subset,
                  learning_rate=config['lr'],
                  train_criterion=loss_dict['cross_entropy'],
                  model_optimizer=opti_dict['adam'],
                  data_augmentations=None,
                  save_model_str=None,
                  )

        return({
            'loss': float(1 - val_score),  # we minimize!
            'info': {
                'train_error': float(1 - train_score),
                'test_error': float(1 - test_score),
            },
        })

    @staticmethod
    def get_configspace():
        ###################### New apporach with cells. Each cells have between 1 and 5 blocks (hidden representation),
        # They can be connected in anyways in acyclic manner.Each cell can be a reduction cell or normal cell
        # where normal cells output the feature maps of same height and width (but not channels) size, and reduction
        # cells output reduced height and width size.
        #TODO: Set defaults correctly!
        cs_cells = CS.ConfigurationSpace()

        # Determine how many cells and how many blocks per cell to have here!
        max_n_cells = 1
        max_n_blocks = 5

        # Hyperparameter for choosing how many cells to use
        if max_n_cells == 1:
            n_cells = CSH.Constant("n_cells", 1)
        else:
            n_cells = CSH.UniformIntegerHyperparameter("n_cells", lower=1, upper=max_n_cells, default_value=1)
        cs_cells.add_hyperparameter(n_cells)

        for i in range(max_n_cells):  # max_num_cells
            # Iteratively create hyperparameters that specify the number of blocks
            n_blocks_in_this_cell = (CSH.UniformIntegerHyperparameter("cell%i_n_blocks" % i, lower=2, upper=max_n_blocks))
            cs_cells.add_hyperparameter(n_blocks_in_this_cell)

            # Condition for activating i-th cell
            cell_condition = CS.InCondition(n_blocks_in_this_cell, n_cells, [cur for cur in range(i+1, max_n_cells+1)])
            cs_cells.add_condition(cell_condition)

            # Define for each layer if it is reduction or not. (Default: First one is reduction with len(B)=2)
            is_reduction = CSH.CategoricalHyperparameter("cell%i_is_reduction" % i, [True, False])
            cs_cells.add_hyperparameter(is_reduction)
            # Condition for activating is_reduction
            cell_condition = CS.InCondition(is_reduction, n_cells, [cur for cur in range(i+1, max_n_cells+1)])
            cs_cells.add_condition(cell_condition)

            # Add more operations here!
            possible_ops = ['conv1', 'conv3', 'conv5', 'max_pool3', 'avg_pool3', 'none', 'identity']

            # Store all operations. ops_list[i] will contain all parent operations of operation_ij
            parent_ops_list = [[] for i in range(max_n_blocks)]
            child_ops_list = [[] for i in range(max_n_blocks)]

            for j in range(1, max_n_blocks+1):
                for k in range(1, j):
                    # operation from k-th block to j-th block (sort of densely connected)
                    op_kj = CSH.CategoricalHyperparameter("cell%i_op_%i%i" % (i, k, j), possible_ops)
                    parent_ops_list[j-1].append(op_kj)
                    child_ops_list[k-1].append(op_kj)
                    cs_cells.add_hyperparameter(op_kj)

                    # Condition for activating operation
                    op_condition = CS.InCondition(op_kj, n_blocks_in_this_cell, [cur for cur in range(j, max_n_blocks+1)])
                    cs_cells.add_condition(op_condition)

                    # number of out channel of each operation k to j
                    op_kj_n_out_channel = CSH.UniformIntegerHyperparameter(
                        "cell%i_op_%i%i_numoutchannel" % (i, k, j), lower=4, upper=64)
                    cs_cells.add_hyperparameter(op_kj_n_out_channel)

                    # Conditions
                    # these parameters exist only if the block is not 'none' or 'identity'
                    # 'none produces no out channels (just returns 0), and 'identity' produces the
                    # same output channel as its input
                    channel_condition2 = CS.InCondition(op_kj_n_out_channel, op_kj, [op for op in possible_ops if op not in ['none', 'identity']])
                    cs_cells.add_condition(channel_condition2)

            # op_ij (operation that goes from i-th block to j-th block) must be 'none' if all its parent ops
            # (op_ki for all k < i) are 'none'.
            #print(parent_ops_list)
            #print(child_ops_list)
            for i in range(1, max_n_blocks):
                for child_op in child_ops_list[i]:
                    forbiddens = [CS.ForbiddenEqualsClause(parent_op, 'none') for parent_op in parent_ops_list[i]]
                    forbiddens.append(CS.ForbiddenInClause(child_op, [op_type for op_type in possible_ops if op_type not in ['none']]))
                    forbidden_clause = CS.ForbiddenAndConjunction(*forbiddens)
                    cs_cells.add_forbidden_clause(forbidden_clause)
                    #print(forbidden_clause)

        # Non-architectural hyperparameters
        # learning rate
        lr = CSH.UniformFloatHyperparameter("lr", lower=1e-5, upper=1e-2, log=True)
        cs_cells.add_hyperparameter(lr)
        # batch_size (because batch size and learning rate must be correlated?)
        batch_size = CSH.CategoricalHyperparameter("batch_size", [16, 32, 64, 128])
        cs_cells.add_hyperparameter(batch_size)

        return(cs_cells)
        ###################################################
