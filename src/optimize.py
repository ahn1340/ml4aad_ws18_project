import torch
import torch.nn as nn
import torch.nn.functional as F
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

class RNN_encoder(nn.Module):
    def __init__(self, input_size, hidden_size):
        super(RNN_encoder, self).__init__()
        self.hidden_size = hidden_size
        self.num_layers = 3
        self.input_size = input_size
        self.rnn = nn.LSTM(input_size, hidden_size, self.num_layers, batch_first=True)
        self.relu = nn.ReLU()

    def forward(self, x):
        h0 = torch.zeros(self.num_layers, x.size(0), self.hidden_size).to(device)
        c0 = torch.zeros(self.num_layers, x.size(0), self.hidden_size).to(device)

        out, (h1, c1) = self.rnn(x, (h0, c0))
        out = self.relu(out)
        #print(out.size())
        #print(h1)
        #print(c1)

        return(out)

class RNN_decoder(nn.Module):
    def __init__(self, hidden_size, output_size):
        super(RNN_decoder, self).__init__()
        self.hidden_size = hidden_size
        self.output_size = output_size
        self.num_layers = 3
        self.rnn = nn.LSTM(hidden_size, output_size, self.num_layers, batch_first=True)
        self.relu = nn.ReLU()

    def forward(self, encoded_x):
        h0 = torch.zeros(self.num_layers, encoded_x.size(0), self.output_size).to(device)
        c0 = torch.zeros(self.num_layers, encoded_x.size(0), self.output_size).to(device)

        out, (h1, c1) = self.rnn(encoded_x, (h0, c0))
        #print(out.size())
        #print(out)
        #print(h1)
        #print(c1)
        return(out)


hidden_size = 2
input_size = 3
output_size = input_size
enc = RNN_encoder(input_size, hidden_size)
dec = RNN_decoder(hidden_size, output_size)

x = torch.Tensor([[1, 0, 0],
                  [0, 0, 1],
                  [0, 1, 0],
                  [0, 0, 0],
                  [0, 0, 0]])
x = x.view(1, 5, 3) # batch, seq_length, num_features

import copy
import torch.optim as optim
optimizer_enc = optim.SGD(enc.parameters(), lr=0.01)
optimizer_dec = optim.SGD(dec.parameters(), lr=0.01)


for i in range(10):
    input = torch.autograd.Variable(x)
    target = torch.autograd.Variable(x)
    #target = target.max(dim=1)
    new_target = copy.deepcopy(target)
    optimizer_enc.zero_grad()
    optimizer_dec.zero_grad()
    pred = enc(x)
    pred = dec(pred)

    print(target.size())
    print(pred.size())
    loss = F.cross_entropy(pred, target)
    print(loss)
    loss.backward()
    optimizer_enc.step()
    optimizer_dec.step()





#print(b.size())
#print(a.forward(x))

