import numpy as np
import torch.nn as nn
from ops import Identity
from cell import SearchCell

# define ConvNet #######################################################################################################
class ConfigurableNet(nn.Module):
    """
    Example of a configurable network. (No dropout or skip connections supported)
    """
    def _update_size(self, dim, padding, dilation, kernel_size, stride):
        """
        Helper method to keep track of changing output dimensions between convolutions and Pooling layers
        returns the updated dimension "dim" (e.g. height or width)
        """
        return int(np.floor((dim + 2 * padding - dilation * (kernel_size - 1) - 1) / stride + 1))

    def __init__(self, config, num_classes=10, height=28, width=28, channels=1):
        """
        Configurable network for image classification
        :param config: network config to construct architecture with
        :param num_classes: Number of outputs required
        :param height: image height
        :param width: image width
        """
        super(ConfigurableNet, self).__init__()
        self.config = config

        ##################################################################3
        # number of cells
        n_cells = config["n_cells"]

        # Initialize Search cells
        self.mymodules = nn.ModuleList()

        # Initial input channel size to the first cell is 1
        cell_n_out_channel = 1

        # Append all cells to module list
        for i in range(n_cells):
            cell = SearchCell(config=self.config, cell_idx=i, cell_n_in_channel=cell_n_out_channel)
            cell_n_out_channel = cell.output_channel_size()
            cell_is_reduction = config["cell%i_is_reduction" % i]
            self.mymodules.append(cell)

            # Reduce the size to half
            #print(cell_is_reduction)
            if cell_is_reduction:
                pool = nn.MaxPool2d(kernel_size=3, stride=2, padding=0)
                height //= 2
                width //= 2
                self.mymodules.append(pool)

        # For now, apply 1*1 convolution and max pooling to dim 4
        last_layer_channel_size = 20
        self.reduce = nn.Conv2d(in_channels=cell_n_out_channel, out_channels=last_layer_channel_size, kernel_size=1, stride=1, padding=0)
        self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, padding=0)
        self.mymodules.append(self.reduce)
        self.mymodules.append(self.maxpool)
        height = int((height - 3) / 2 + 1)
        width = int((width - 3) / 2 + 1)
        print(height)
        print(width)

        # Append final fully connected layer
        self.fc = nn.Linear(last_layer_channel_size * height * width, num_classes)

    def compute_stride_size(self, filter_size, height):
        # Function to calculate stride size to make final layer to have height and width (4, 4)
        # Assumes height and width are the same.
        s = (height - filter_size) / 3
        print(s)

    def forward(self, out):
        for idx, cell in enumerate(self.mymodules):
            out = cell(out)
        # Flatten the output for final fc layer
        out = out.reshape(out.size(0), -1)
        out = self.fc(out)
        return(out)



